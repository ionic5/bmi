import { Component } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  private height: number;
  private weight: number;
  private bmi: number;

  constructor() {
    this.height = 0;
    this.weight = 0;
    this.bmi = 0;
  }

  private calculate(){ 
    this.bmi = this.weight / (Math.pow(this.height,2));
  }

}
